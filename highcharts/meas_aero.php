<?php 
# - Include config file to get the color definition
require('../config.php');
?>
<!DOCTYPE html>
<html>
<head>

<script src='js/jquery-1.10.2.min.js'></script>
<script src='js/highcharts.js'></script>

<style type='text/css'>
body {
  background-color: black;
}
div#container {
  display: block;
  width: 750px;
  margin-left: -375px;
  height: 550px;
  margin-top: -285px;
  position: absolute;
  left: 50%;
  top: 50%;
}
</style>

<script type='text/javascript'>
$(document).ready(function(){

  // - Initialize new highcarts object
  $.chart =  new Highcharts.Chart({
    chart: {
        renderTo: 'container',
        spacingRight: 20,
        backgroundColor:'rgba(0,0,0,0.1)',
        margin: [50,10,80,90],
    },
    title: {
        text: 'Aerosolgehalt',
        style: { color: '#fff', fontWeight: 'bold', fontSize: '30px' },
    },
    xAxis: {
      type: 'datetime',
      title: {
          text: null
      },
      labels: {
        y: +30,
        align: 'right',
        rotation: -90,
        style: {
          color: '#fff',
          fontSize: '18px',
        }   
      },
    },
    yAxis: {
      title: {
        text: 'Aerosolgehalt [Mikdrogram/kg]',
        style: {
          color: '#fff',
          fontSize: '20px',
        }
      },
      labels: {
        formatter: function() {
          return this.value;
        },
        style: {
          color: '#fff',
          fontSize: '18px',
        }   
      },
      gridLineWidth: '2px',
    },
    legend: { enabled: false, },
  });

  // - Loading data via .ajax
  console.log('x');
  $.ajax({
    url: "../getjson.php", 
    data: {'what':'meas'},
    dataType: "json",
    async: false,
    success: function(data) {
      console.log($.jsondata);
      $.jsondata = data
      var series = {
        id: 'meas_aero',
        name: 'Aerosole',
        color: '<?php print $hjcolors['aero']; ?>',
        lineWidth: '8px',
        data: $.jsondata['meas_aero'],
      }
      $.chart.addSeries(series);
    }
  });

});
</script>

</head>
<body>
  <div id='container'></div>
</body>
</html>
