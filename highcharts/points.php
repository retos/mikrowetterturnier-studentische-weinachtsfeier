<?php 
# - Include config file to get the color definition
require('../config.php');

# - If $_GET is empty: take default (best 5)
if ( empty($_GET) ) {
  $what = 'bar_best';
  $nn   = $json_ndefault;
} else {
  $what = 'bar_best'; # default
  $nn   = $json_ndefault;
  # - If user settings: use them
  if ( ! empty($_GET['what']) ) { $what = $_GET['what']; }
  if ( ! empty($_GET['nn']) )   { $nn   = (int)$_GET['nn']; }
}
?>
<!DOCTYPE html>
<html>
<head>

<script src='js/jquery-1.10.2.min.js'></script>
<script src='js/highcharts.js'></script>

<style type='text/css'>
body {
  background-color: black;
}
div#container {
  display: block;
  width: 750px;
  margin-left: -375px;
  height: 550px;
  margin-top: -285px;
  position: absolute;
  left: 50%;
  top: 50%;
}
</style>

<script type='text/javascript'>
$(document).ready(function(){

  var nn = '<?php print $nn; ?>';
  var what = '<?php print $what; ?>';

  var title = 'Die Besten ';
  if      ( what == 'bar_worst' )  { title = 'Die Schlechtesten '; }
  else if ( what == 'bar_random' ) { title = 'Zufaellig '; }

  // - Rotation of the labels
  var rotation = 0;
  var alignment = 'center';
  if ( parseInt(nn) > 5 ) {
    rotation = -45;
    alignment = 'right';
  }

  // - Loading data via .ajax
  $.ajax({
    url: "../getjson.php", 
    data: {'what':what,'n':nn},
    dataType: "json",
    async: false,
    success: function(data) {
      $.jsondata = data
    }
  });

  // - Initialize new highcarts object
  $.chart =  new Highcharts.Chart({
    chart: {
        renderTo: 'container',
        backgroundColor:'rgba(0,0,0,0.1)',
        margin: [50,10,120,90],
        type: 'column',
    },
    title: {
        text: title+' '+nn+': Gesamtpunkte', 
        style: { color: '#fff', fontWeight: 'bold', fontSize: '30px' },
    },
    xAxis: {
      categories: $.jsondata.name, 
      labels: {
        y: +30,
        align: alignment, 
        rotation: rotation,
        style: {
          color: '#fff',
          fontSize: '18px',
        }   
      },
    },
    yAxis: {
      title: {
        text: 'Gesamtpunktzahl',
        style: {
          color: '#fff',
          fontSize: '20px',
        }
      },
      labels: {
        align: 'right',
        rotation: rotation,
        style: {
          color: '#fff',
          fontSize: '18px',
        }   
      },
      min: 0,
      max: 100,
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
          align: 'center',
          x: 10,
          rotation: -90,
          style: {
            color: '#000',
            fontSize: '35px',
          }
        },
      },
    },
    legend: {
      enabled: false,
    },
    //series: $.jsondata.data,
  });
  var series  = {
    color: '<?php print $hjcolors['total']; ?>', data: $.jsondata.p_total,
    name: 'Temperatur',
  }
  $.chart.addSeries(series);


});
</script>

</head>
<body>
  <div id='container'></div>
</body>
</html>
