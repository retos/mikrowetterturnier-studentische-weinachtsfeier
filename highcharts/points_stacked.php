<?php 
# - Include config file to get the color definition
require('../config.php');

# - If $_GET is empty: take default (best 5)
if ( empty($_GET) ) {
  $what = 'bar_best';
  $nn   = $json_ndefault;
} else {
  $what = 'bar_best'; # default
  $nn   = $json_ndefault;
  # - If user settings: use them
  if ( ! empty($_GET['what']) ) { $what = $_GET['what']; }
  if ( ! empty($_GET['nn']) )   { $nn   = (int)$_GET['nn']; }
}
?>
<!DOCTYPE html>
<html>
<head>

<script src='js/jquery-1.10.2.min.js'></script>
<script src='js/highcharts.js'></script>

<style type='text/css'>
body {
  background-color: black;
}
div#container {
  display: block;
  width: 750px;
  margin-left: -375px;
  height: 550px;
  margin-top: -285px;
  position: absolute;
  left: 50%;
  top: 50%;
}
</style>

<script type='text/javascript'>
$(document).ready(function(){

  var nn = '<?php print $nn; ?>';
  var what = '<?php print $what; ?>';

  // - Rotation of the labels
  var rotation = 0;
  if ( parseInt(nn) > 5 ) {
    rotation = -12
  }

  title = 'Die Besten';
  if ( what == 'bar_worst' ) { title = 'Die Schlechtesten'; }
  else if ( what == 'bar_random' ) { title = 'Zufaellig'; }

  // - Loading data via .ajax
  $.ajax({
    url: "../getjson.php", 
    data: {'what':what,'n':nn},
    dataType: "json",
    async: false,
    success: function(data) {
      $.jsondata = data
    }
  });

  // - Initialize new highcarts object
  $.chart =  new Highcharts.Chart({
    chart: {
        renderTo: 'container',
        backgroundColor:'rgba(0,0,0,0.1)',
        margin: [50,10,120,90],
        type: 'column',
    },
    title: {
        text: title+' '+nn+': Teilpunkte',
        style: { color: '#fff', fontWeight: 'bold', fontSize: '30px' },
    },
    xAxis: {
      categories: $.jsondata.name, 
      labels: {
        y: +30,
        align: 'right',
        rotation: rotation,
        style: {
          color: '#fff',
          fontSize: '18px',
        }   
      },
    },
    yAxis: {
      title: {
        text: 'Aerosolgehalt [Mikdrogram/kg]',
        style: {
          color: '#fff',
          fontSize: '20px',
        }
      },
      labels: {
        align: 'right',
        rotation: rotation,
        style: {
          color: '#fff',
          fontSize: '18px',
        }   
      },
      min: 0,
    },
    plotOptions: { column: { stacking: 'normal',  }, },
    legend: {
      enabled: true,
      style: {
        color: '#fff',
        fontSize: '15px',
      },
      backgroundColor: '#fff',
    },
    //series: $.jsondata.data,
  });
  var series_T  = {
    color: '<?php print $hjcolors['T']; ?>', data: $.jsondata.p_T,
    name: 'Temperatur',
  }
  var series_rh = {
    color: '<?php print $hjcolors['rh']; ?>', data: $.jsondata.p_rh,
    name: 'Rel. Feuchte',
  }
  var series_beer = {
    color: '<?php print $hjcolors['beer']; ?>', data: $.jsondata.p_beer,
    name: 'Bierkonsum',
  }
  var series_aero = {
    color: '<?php print $hjcolors['aero']; ?>', data: $.jsondata.p_aero,
    name: 'Aerosolgehalt',
  }
  $.chart.addSeries(series_T);
  $.chart.addSeries(series_rh);
  $.chart.addSeries(series_beer);
  $.chart.addSeries(series_aero);


});
</script>

</head>
<body>
  <div id='container'></div>
</body>
</html>
