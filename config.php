<?php
# - Here you can find some configuration parameters
#   e.g., the name of the sqlite file, the names of the
#   database tables, parameters to control the scoring
#   and so on.


# - Setting directory paths. reldir ist the relative
#   path on the webserver (from httpd/http). The
#   absdir is the webserver full path (disc path).
$reldir = "mikro";
$absdir = $_SERVER["DOCUMENT_ROOT"]."/".$reldir; 


# - Name and location of the sqlite database. The name
#   of the database is the current year by default!
#   You can also use another file by changing the
#   name.
$DBdir  = "sqlite";
$DBname = "mikrowetterturnier_".date('Y').".sqlite";


# - Table configuration. If the sqlite3 file is not existing,
#   create those tables inside the newly create sqlite database.
$DBtable_tips = "CREATE TABLE tips ("
               ."  `name` text NOT NULL PRIMARY KEY, "
               ."  `T`       real, "
               ."  `rh`      real, "
               ."  `beer`    real, "
               ."  `aero`    real, "
               ."  `p_T`     real, "
               ."  `p_rh`    real, "
               ."  `p_beer`  real, "
               ."  `p_aero`  real, "
               ."  `p_total` real, "
               ."  `ranking` int, "
               ."  `typ`  int "
               .");";
$DBtable_meas = "CREATE TABLE meas ("
               ."  `time` int NOT NULL, "
               ."  `T`    real, "
               ."  `rh`   real, "
               ."  `beer` real, "
               ."  `aero` real, "
               ."  `typ`  int "
               .");";
$DBtables = array('tips'=>$DBtable_tips,'meas'=>$DBtable_meas);


# - Time-Converter. Time for measurements is always given as
#   HH:MM. Without the date. But we need a valid timestamp
#   to store the data. therefore we need something special here.
#   If: current hour < inserted hour: take "yesterday".
#         e.g.: current is 00:06, insert is 23:30 -> measurement
#               from "yesterday"
#   Else use current date.
$timecontrol = 6;


# - Maximum number of points per variable
#   and slope. Slope of the linear ranking function
#   e.g. 3 is meaning that after 3x the standard 
#   deviation you are earning 0 points.
$maxpoints = 100;
$slope     = 3;


# - This is for the getjson file. Here you can specify
#   the "default number of shown ..." (points, players, ...).
#   You can also control this value via an input to the
#   getjson.php file (see header description).
$json_ndefault = 5;
#   The same holds for the default "what" input to getjson.php
$json_whatdefault = 'best';


# - Highcharts: colors defined here!
$hjcolors = array('T'=>'#FFA7D1',
                  'beer'=>'#FFC848',
                  'rh'=>'#B6E818',
                  'aero'=>'#00FAB5',
                  'total'=>'#81F7F3',
                 );


# - There are two different screen-display-scripts.
#   Here you can define which page should occure for
#   how long.
#   The speed variable controls how long the slides should be shown.
$screen_tables_speed = 15; # in seconds. 
$screen_tables = array(
                  'showtable.php?what=bar_random&nn=10',
                  'showtable.php?what=bar_worst',
                  'showtable.php?what=bar_random&nn=10',
                  'showtable.php?what=bar_best',
                 );

$screen_meas_speed = 10; # in seconds. 
$screen_meas = array(
                  'highcharts/meas_T.php',
                  'highcharts/meas_rh.php',
                  'highcharts/meas_beer.php',
                  'highcharts/meas_aero.php',
               );

$screen_measfoto_speed = 10; # in seconds. 
$screen_measfoto = array(
                  'highcharts/meas_T.php',
                  'highcharts/meas_rh.php',
                  'fotos.php?n=5',
                  'highcharts/meas_beer.php',
                  'highcharts/meas_aero.php',
                  'fotos.php?n=5',
                   );

$screen_all_speed = 15; # in seconds. 
$screen_all = array(
                  'highcharts/points.php?what=bar_random',
                  'showtable.php?what=bar_random&nn=10',
                  'highcharts/meas_T.php',
                  'highcharts/points.php?what=bar_random',
                  'showtable.php?what=bar_random&nn=10',
                  'highcharts/points.php?what=bar_worst',
                  'highcharts/meas_rh.php',
                  'showtable.php?what=bar_worst',
                  'highcharts/meas_beer.php',
                  'highcharts/points.php?what=bar_best',
                  'showtable.php?what=bar_best',
                  'highcharts/meas_aero.php',
               );

# - Configuration for the pardeeee images. You can put
#   any valid image into the directory defined below.
#   pardeeee.php will then display random images for
#   a 800x600 resolution.

$imagesdir = $absdir."/fotos";
$relimagesdir = "/".$reldir."/fotos";
# - Note: only those image files are valid.
#   Not case sensitive (png == PNG) 
$valid_images = array('png','jpg','jpeg','gif'); 
# - For the flux image presenter: the delay
$imagedelay = 5; # seconds
$numberofimages = 5; # default number of images to load. This
                     # is useless for the fotos.php?n=all option.

?>
