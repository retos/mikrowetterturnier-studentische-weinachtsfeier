<?php
# - Need necessary includes
require('config.php');
require('function.php');

# - Open database, calling computepoints() function. Thats all :)
$DBcon = DBconnect();
computepoints();
DBclose();

# - Back to the index
Header("Location: ./");
?>
