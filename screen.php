<?php 
# - Include config file to get the color definition
require('config.php');

$what = 'meas';
if ( ! empty($_GET) ) { 
  if ( ! empty($_GET['what']) ) { $what = $_GET['what']; }
}
?>
<!DOCTYPE html>
<html>
<head>

<style type='text/css'>
body {
  background-color: black;
}

#iframe {
  display: block;
  position: absolute;
  left: 50%;
  top: 50%;
  width: 760px;
  margin-left: -380px;
  height: 560px;
  margin-top: -280px;
  border: none;
  padding: none;
  margin: none;
}
</style>

<script src='js/jquery-1.10.2.min.js'></script>

<script type='text/javascript'>
$(document).ready( function() {

  var what = '<?php print $what; ?>';
  // - Loading page configuration
  $.ajax({
    url: "screen_json.php", 
    data: {'what':what},
    dataType: "json",
    async: false,
    success: function(data) {
      $.jsondata = data
    }
  });

  // - Helper function to display the page
  $.counter = 0;
  $.showpage = function() {
    $('#iframe').attr('src',$.jsondata.pages[$.counter])
    $.counter++;
    if ( $.counter >= $.jsondata.pages.length ) { $.counter = 0 };
  }

  $.showpage();
  $.screeninterval = window.setInterval($.showpage,$.jsondata.speed*1000);

});

</script>

</head>
<body>
<iframe id='iframe'></iframe>
</body>
</html>
