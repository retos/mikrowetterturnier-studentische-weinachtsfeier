<?
# - Define the 
global $dbt_tips;
global $dbt_meas;
global $absdir;
global $reldir;

// -- Slope of the linear ranking function
//    e.g. 5 is meaning that after 5x the standard 
//    deviation you are earning 0 points.
$slope = 3;

// -- Maximum points if you are hitting the meassurement
$maxpoints = 100;


# -------------------------------------------------------------------
# - The function to insert a new tip into the database.
#   Returns a $message array as "result".
# ----------
function inserttip()
{
  global $_POST;
  global $DBcon; # database connection

  # - If name not set: stop
  if ( empty($_POST['name']) ) {
    $message = "ERROR: No name set. Entry not stored yet!";
    $message_class = 'error';
  } else {
    # - Checking if name allready in database: stop.
    $num = $DBcon->querySingle("SELECT COUNT(*) AS count FROM tips "
                              ."WHERE name='".htmlspecialchars($_POST['name'])."';");
    # - If there is an entry with the same name: stop
    if ( $num > 0 )
    {
      $message = "ERROR: Name <<b>".htmlspecialchars($_POST['name'])."</b>> "
                ."allready set in database! Take another one. "
                ."This one is not unique!";
      $message_class = 'error';
    # - Checking if there are special characters in the name. If it is: stop!
    } else if ( strcmp($_POST['name'],htmlspecialchars($_POST['name'])) != 0 ) {
      $message = "ERROR: Sorry, special characters are not allowed (such stuff as "
                .htmlspecialchars("$,&,%,#,...")."). Dont store. Please change the name.";
      $message_class = 'error';
    # - Else store tip into the database
    } else {
      # - Getting values: Note: if somethig is empty we need to 
      #   set it to -999, else the sqlite3 command wont work propperly.
      $tip_name = htmlspecialchars($_POST['name']);
      $tip_T       = -999;
      if ( ! empty($_POST['T']) ) { $tip_T = $_POST['T']; }
      $tip_rh      = -999;
      if ( ! empty($_POST['rh']) ) { $tip_rh = $_POST['rh']; }
      $tip_beer    = -999;
      if ( ! empty($_POST['beer']) ) { $tip_beer = $_POST['beer']; }
      $tip_aero    = -999;
      if ( ! empty($_POST['aero']) ) { $tip_aero = $_POST['aero']; }
      $tip_typ    = -999;
      if ( ! empty($_POST['typ']) ) { $tip_typ = $_POST['typ']; }

      # - Generating sqlite3 command
      $cmd = "INSERT INTO tips (name,T,rh,beer,aero,typ) VALUES "
            ."('".$tip_name."', ".$tip_T.", ".$tip_rh.", "
            .$tip_beer.", ".$tip_aero.", ".$tip_typ.");";
      #print $cmd;
      # - Now insert into the database
      $DBcon->exec($cmd);
      # - NOTE: KILL $_POST VARIABLE!
      $_POST = NULL;

      # - Screen message
      $message = "New entry stored into the database.";
      $message_class = 'ok';
    }
  }
  return(array("text"=>$message,"class"=>$message_class));
}

# -------------------------------------------------------------------
# - If a $_POST[$what] variable is not empty, the function prints
#   the value of this variable. Used to re-fill the form input fields
#   if there was an error, so that the user dont has to insert
#   all fields again.
# ----------
function showval($what) {
  global $_POST;
  if ( ! empty($_POST[$what]) ) {
    print $_POST[$what];
  }
}

# -------------------------------------------------------------------
# - Insert measurement into the database. Returns a $message array
#   as "result", nothing else.
# ----------
function insertmeas() {
  # - Checking "time". Necessary.
  if ( empty($_POST['mtime']) ) {
    $message = "ERROR: no time set (format HH:MM). Dont store anything.";
    $message_class = "error";
  # - Checking time format
  } else {
    # - Try to split at ":"
    $parts = split(":",$_POST['mtime']);
    if ( count($parts) != 2 ) {
      $message = "ERORR: your time seems to have wrong format (HH:MM). Nothing stored.";
      $message_class = "error";
    # - Parse time and store values into the database
    } else  {
      global $timecontrol;
      global $DBcon;
      $hour = (int)$parts[0];
      $min  = (int)$parts[1];
      $curr_hour = (int)date('H');
      # - If current hour < hour and hour > $timecontrol:
      #   a measurement from yesterday.
      if ( (int)$curr_hour < (int)$hour and (int)$curr_hour < (int)$timecontrol ) {
        $date = date('Y-m-d',time()-86400);
        $stamp = strtotime($date." 00:00") + $hour*3600 + $min*60;
      } else {
        $date = date('Y-m-d');
        $stamp = strtotime($date." 00:00") + $hour*3600 + $min*60;
      }
      # - Checking if time allready in database: stop.
      $num = $DBcon->querySingle("SELECT COUNT(*) AS count FROM meas WHERE time=".$stamp.";");
      # - If there is an entry with the same name: stop
      if ( $num > 0 )
      {
        $message = "ERROR: Measurement for <<b>".date('Y-m-d H:i:s',$stamp)."</b>> "
                  ."allready set in database! This one is not unique, dont save!<br><br>"
                  ."Debug: current_hour ".$curr_hour."; hour ".$hour."; timecontrol ".$timecontrol;
        $message_class = 'error';
      } else {
        # - Create sql command first (note: need -999, missing values are not allowed
        #   for sqlite
        $mtime = $_POST['mtime']; 
        $mT    = -999;     if ( ! empty($_POST['mT'])    ) { $mT    = $_POST['mT'];    }
        $mrh   = -999;     if ( ! empty($_POST['mrh'])   ) { $mrh   = $_POST['mrh'];   }
        $mbeer = -999;     if ( ! empty($_POST['mbeer']) ) { $mbeer = $_POST['mbeer']; }
        $maero = -999;     if ( ! empty($_POST['maero']) ) { $maero = $_POST['maero']; }
        $cmd = "INSERT INTO meas (time,T,rh,beer,aero) VALUES (".$stamp.",".$mT.",".$mrh.", "
              .$mbeer.",".$maero.");";
        #print $cmd."<br>";
        $DBcon->exec($cmd);

        # - NOTE: KILL $_POST VARIABLE!
        $_POST = NULL;

        # - Return message
        $message = "Stored measurement on: ".date('Y-m-d H:i:s',$stamp); 
        $message_class = 'ok';
      }
    }
  }
  return(array("text"=>$message,"class"=>$message_class));
}

# -------------------------------------------------------------------
# - Show measurements as a list
# -----------
function showmeas() {
  global $DBcon;
  $rows = $DBcon->query("SELECT * FROM meas ORDER BY time DESC");
  
  # - Title
  echo "<h3>Messungen in der Datenbank</h3>";

  # - Table header
  echo "<table>\n"
      ."  <tr>\n"
      ."    <th>Datetime</th>\n"
      ."    <th>T</th>\n"
      ."    <th>rh</th>\n"
      ."    <th>beer</th>\n"
      ."    <th>aero</th>\n"
      ."    <th>delete</th>\n"
      ."  </tr>\n";
  # - Plotting data
  $counter = 0;
  while ( $row=$rows->fetchArray() ) {
     echo "  <tr>\n"
         ."    <td class='tdc'>".date('Y-m-d H:i',$row['time'])."</td>\n"
         ."    <td class='tdr'>".sprintf('%10.2f',$row['T'])."</td>\n"
         ."    <td class='tdr'>".sprintf('%10.2f',$row['rh'])."</td>\n"
         ."    <td class='tdr'>".sprintf('%10.2f',$row['beer'])."</td>\n"
         ."    <td class='tdr'>".sprintf('%10.2f',$row['aero'])."</td>\n"
         ."    <td class='tddel'><a href='delete.php?type=meas&time=".$row['time']."'>delete</a></td>\n"
         ."  </tr>\n"; 
     $counter++;
  }
  # - Table footer
  echo "</table>\n";

  # - Empty?
  if ( $counter == 0 ) {
    echo "Table seems to be empty, nothing stored yet.\n";
  }

}

# -------------------------------------------------------------------
# - Show measurements as a list
# -----------
function showtips() {
  global $DBcon;
  global $_GET;
  if ( ! empty($_GET['points']) ) { $showpoints = true; } else { $showpoints = false; }

  $rows = $DBcon->query("SELECT * FROM tips ORDER BY name ASC");
  
  # - Titpe
  echo "<h3>Tips in der Datenbank</h3>\n";
  echo "[<a href='".$_SERVER['PHP_SELF']."'>Tips Anzeigen</a>]\n";
  echo "&nbsp;&nbsp;\n";
  echo "[<a href='".$_SERVER['PHP_SELF']."?points=t'>Punkte Anzeigen</a>]\n";


  # - Table header
  if ( ! $showpoints ) {
    $tablehead = array("Name","T","rh","beer","aero","typ","delete");
    $tablecol  = array("name","T","rh","beer","aero","typ");
  } else {
    $tablehead = array("Name","p(T)","p(rh)","p(beer)","p(aero)","p(mean)","delete");
    $tablecol  = array("name","p_T","p_rh","p_beer","p_aero","p_total");
  }
  echo "<table>\n"
      ."  <tr>\n"
      ."    <th>RK</th>\n"
      ."    <th>".$tablehead[0]."</th>\n"
      ."    <th>".$tablehead[1]."</th>\n"
      ."    <th>".$tablehead[2]."</th>\n"
      ."    <th>".$tablehead[3]."</th>\n"
      ."    <th>".$tablehead[4]."</th>\n"
      ."    <th>".$tablehead[5]."</th>\n"
      ."    <th>delete</th>\n"
      ."  </tr>\n";
  # - Plotting data
  $counter = 0;
  while ( $row=$rows->fetchArray() ) {
     $counter++;
     echo "  <tr>\n"
         ."    <td class='tdc' style='width: 5px;'><b>".$row['ranking']."</b></td>\n"
         ."    <td class='tdl'>".$row[$tablecol[0]]."</td>\n"
         ."    <td class='tdr'>".sprintf('%10.1f',$row[$tablecol[1]])."</td>\n"
         ."    <td class='tdr'>".sprintf('%10.1f',$row[$tablecol[2]])."</td>\n"
         ."    <td class='tdr'>".sprintf('%10.1f',$row[$tablecol[3]])."</td>\n"
         ."    <td class='tdr'>".sprintf('%10.1f',$row[$tablecol[4]])."</td>\n"
         ."    <td class='tdr'>[".sprintf('%10.1f',$row[$tablecol[5]])."]</td>\n"
         ."    <td class='tddel'><a href='delete.php?type=tips&name=".$row['name']."'>delete</a></td>\n"
         ."  </tr>\n"; 
  }
  # - Table footer
  echo "</table>\n";

  # - Empty?
  if ( $counter == 0 ) {
    echo "Table seems to be empty, nothing stored yet.\n";
  }
}



###  old  ### function computerank($mrow)
###  old  ### {
###  old  ###   global $dbt_tips;
###  old  ###   global $slope;
###  old  ###   global $maxpoints;
###  old  ### 
###  old  ###   // -- Loading standard deviation from database
###  old  ###   $stdresult = mysql_query("SELECT stddev(T) AS stdT, "
###  old  ###                           ." avg(T) AS meanT, "
###  old  ###                           ." stddev(rh) AS stdrh, "
###  old  ###                           ." avg(rh) AS meanrh, "
###  old  ###                           ." stddev(beer) AS stdbeer, "
###  old  ###                           ." avg(beer) AS meanbeer, "
###  old  ###                           ." stddev(aero) AS stdaero, "
###  old  ###                           ." avg(aero) AS meanaero "
###  old  ###                           ." FROM ".$dbt_tips
###  old  ###                           ." WHERE active=1");
###  old  ###   $stdrow   = mysql_fetch_array($stdresult); 
###  old  ### 
###  old  ###   $stdT     = $stdrow['stdT'];
###  old  ###   $meanT    = $stdrow['meanT'];
###  old  ###   $stdrh    = $stdrow['stdrh'];
###  old  ###   $meanrh   = $stdrow['meanrh'];
###  old  ###   $stdbeer  = $stdrow['stdbeer'];
###  old  ###   $meanbeer = $stdrow['meanbeer'];
###  old  ###   $stdaero  = $stdrow['stdaero'];
###  old  ###   $meanaero = $stdrow['meanaero'];
###  old  ### 
###  old  ### 
###  old  ###   // -- Loading users data, compute points
###  old  ###   $query = "SELECT * FROM ".$dbt_tips." WHERE active=1 ORDER BY ID ASC";
###  old  ###   $result = mysql_query($query);
###  old  ###   $numrow = mysql_num_rows($result);
###  old  ### 
###  old  ###   // -- Compute the shit if numrow is greater than 0
###  old  ###   if($numrow>0)
###  old  ###   {
###  old  ###     for($i=1;$i<=$numrow;$i++)
###  old  ###     {
###  old  ###       $row = mysql_fetch_array($result);
###  old  ### 
###  old  ###       // -- Compute points
###  old  ###       //    A) for temperature
###  old  ###       $diff = abs($mrow['T'] - $row['T']) / $stdT;  // x * stdT
###  old  ###       $pT      = $maxpoints - $diff/$slope*10;
###  old  ### 
###  old  ###       //    B) for relative humidity
###  old  ###       $diff = abs($mrow['rh'] - $row['rh']) / $stdrh;  // x * stdrh
###  old  ###       $prh     = $maxpoints - $diff/$slope*10;
###  old  ### 
###  old  ###       //    C) for beer
###  old  ###       $diff = abs($mrow['beer'] - $row['beer']) / $stdbeer;  // x * stdbeer
###  old  ###       $pbeer   = $maxpoints - $diff/$slope*10;
###  old  ### 
###  old  ###       //    D) for aerosol concentration
###  old  ###       $diff = abs($mrow['aero'] - $row['aero']) / $stdaero;  // x * stdaero
###  old  ###       $paero   = $maxpoints - $diff/$slope*10;
###  old  ### 
###  old  ###       // Summating points
###  old  ###       if($pT<0)    { $pT    = 0; } 
###  old  ###       if($prh<0)   { $prh   = 0; } 
###  old  ###       if($pbeer<0) { $pbeer = 0; } 
###  old  ###       if($paero<0) { $paero = 0; } 
###  old  ###       $points = ( $pT + $prh + $pbeer + $paero ) / 4; 
###  old  ### 
###  old  ###       // Write into the database
###  old  ###       mysql_query("UPDATE ".$dbt_tips
###  old  ###                  ." SET points=".$points.", "
###  old  ###                  ." pT=".$pT.", "
###  old  ###                  ." prh=".$prh.", "
###  old  ###                  ." pbeer=".$pbeer.", "
###  old  ###                  ." paero=".$paero
###  old  ###                  ." WHERE ID=".$row['ID']);
###  old  ### 
###  old  ###     } // End of loop
###  old  ### 
###  old  ###   }
###  old  ###   else
###  old  ###   {
###  old  ###     echo "There are no entries in the Database!";
###  old  ###   }
###  old  ### 
###  old  ### }

# -------------------------------------------------------------------
# - Connecting to the sqlite database. If not existing, 
#   create a new one. The table configuration is stored
#   in the config.php file.
# ----------
function DBconnect()
{
    global $DBname;
    global $DBdir;
    global $DBtables;
    # - Checking if file is existing. If not, we have to 
    #   insert the tables we need!
    $check = file_exists($DBdir."/".$DBname);
    $DBcon = new SQLite3($DBdir."/".$DBname); 
    # - Adding database tables
    if ( $check == false ) {
      #$DBcon->exec('CREATE TABLE test (id int);');
      foreach ( array_keys($DBtables) as $val ) {
        #print $DBtables[$val]."<br><br>";
        $DBcon->exec($DBtables[$val]);
      }
    }
    return($DBcon);
}

# -------------------------------------------------------------------
# - Close sqlite3 database connection
# ---------
function DBclose()
{
  global $DBcon;
  $DBcon->close();
}

function int_yyyymmdd_to_engldate($filedate,$hour)
{
  $year = substr($filedate,0,4);
  $month = substr($filedate,4,2);
  $day = substr($filedate,6,2);
  $engldate = $year;
  $engldate = $year."-".$month."-".$day." ".$hour.":00";
  // if($month<10) { $engldate = trim($engldate)."-0".$month; }
  // else          { $engldate = trim($engldate)."-".$month; }
  // if($day<10)   { $engldate = trim($engldate)."-0".$day; }
  // else          { $engldate = trim($engldate)."-".$day; }
  // if($hour<10)  { $engldate = trim($engldate)." 0".$hour.":00"; }
  // else          { $engldate = trim($engldate)." ".$hour.":00"; }
  return $engldate;
}

function int_yyyymmdd_hh_to_timestamp($date,$hour)
{
  $year = substr($date,0,4);
  $mon  = substr($date,4,2);
  $day  = substr($date,6,2);
  $timestamp = mktime($hour,0,0,$mon,$day,$year);
  return $timestamp;
}

#   old  # function show_tips()
#   old  # {
#   old  #   global $dbt_tips;
#   old  # 
#   old  #   // -- Read entries
#   old  #   $query = "SELECT * FROM ".$dbt_tips." WHERE active=1 ORDER BY ID ASC";
#   old  #   $result = @mysql_query($query);
#   old  #   $numrow = @mysql_num_rows($result);
#   old  #   
#   old  #   // -- If there are entries -> show
#   old  #   if($numrow>0)
#   old  #   {
#   old  #     // -- Table header
#   old  #     echo"<table>\n"
#   old  #        ."  <thead>\n"
#   old  #        ."    <tr>\n"
#   old  #        ."      <td>Name</td>\n"
#   old  #        ."      <td>Temp</td>\n"
#   old  #        ."      <td>RelHum</td>\n"
#   old  #        ."      <td>Bier</td>\n"
#   old  #        ."      <td>Feinp.</td>\n"
#   old  #        ."      <td>Punkte</td>\n"
#   old  #        ."      <td>P(T)</td>\n"
#   old  #        ."      <td>P(rh)</td>\n"
#   old  #        ."      <td>P(beer)</td>\n"
#   old  #        ."      <td>P(aero)</td>\n"
#   old  #        ."    </tr>\n"
#   old  #        ."  </thead>\n"
#   old  #        ."  </tbody>\n";
#   old  #   
#   old  #     // -- Loop over all eintries
#   old  #     for($i=1;$i<=$numrow;$i++)
#   old  #     {
#   old  #   
#   old  #       // -- Fetch new database line
#   old  #       $row = mysql_fetch_array($result);
#   old  #       // -- Show entry
#   old  #       echo"    <tr>\n"
#   old  #          ."      <td>".$row['name']."</td>\n"
#   old  #          ."      <td>".$row['T']."</td>\n"
#   old  #          ."      <td>".$row['rh']."</td>\n"
#   old  #          ."      <td>".$row['beer']."</td>\n"
#   old  #          ."      <td>".$row['aero']."</td>\n"
#   old  #          ."      <td>".$row['points']."</td>\n"
#   old  #          ."      <td>".$row['pT']."</td>\n"
#   old  #          ."      <td>".$row['prh']."</td>\n"
#   old  #          ."      <td>".$row['pbeer']."</td>\n"
#   old  #          ."      <td>".$row['paero']."</td>\n"
#   old  #          ."    </tr>\n";
#   old  #   
#   old  #     } // End of loop
#   old  #   
#   old  #     // -- Table footer
#   old  #     echo"  </tbody>\n"
#   old  #        ."</table>\n";
#   old  #   }
#   old  # }


#  old  #  function show_meas()
#  old  #  {
#  old  #    global $dbt_meas;
#  old  #  
#  old  #    // -- Read entries
#  old  #    $query = "SELECT * FROM ".$dbt_meas." WHERE active=1 ORDER BY ID DESC";
#  old  #    $result = @mysql_query($query);
#  old  #    $numrow = @mysql_num_rows($result);
#  old  #    
#  old  #    // -- If there are entries -> show
#  old  #    if($numrow>0)
#  old  #    {
#  old  #      // -- Table header
#  old  #      echo"<table>\n"
#  old  #         ."  <thead>\n"
#  old  #         ."    <tr>\n"
#  old  #         ."      <td>Temp</td>\n"
#  old  #         ."      <td>RelHum</td>\n"
#  old  #         ."      <td>Bier</td>\n"
#  old  #         ."      <td>Feinp.</td>\n"
#  old  #         ."      <td>Zeitpunkt</td>\n"
#  old  #         ."    </tr>\n"
#  old  #         ."  </thead>\n"
#  old  #         ."  </tbody>\n";
#  old  #    
#  old  #      // -- Loop over all eintries
#  old  #      for($i=1;$i<=$numrow;$i++)
#  old  #      {
#  old  #    
#  old  #        // -- Fetch new database line
#  old  #        $row = mysql_fetch_array($result);
#  old  #        // -- Show entry
#  old  #        echo"    <tr>\n"
#  old  #           ."      <td>".$row['T']."</td>\n"
#  old  #           ."      <td>".$row['rh']."</td>\n"
#  old  #           ."      <td>".$row['beer']."</td>\n"
#  old  #           ."      <td>".$row['aero']."</td>\n"
#  old  #           ."      <td>".$row['time']."</td>\n"
#  old  #           ."    </tr>\n";
#  old  #    
#  old  #      } // End of loop
#  old  #    
#  old  #      // -- Table footer
#  old  #      echo"  </tbody>\n"
#  old  #         ."</table>\n";
#  old  #    }
#  old  #  }

# -------------------------------------------------------------------
# - Compute points for all variables, make p_total (mean of all other
#   4 variables) and re-compute the ranking
# ----------
function computepoints() {

  global $DBcon;
  global $maxpoints;
  global $slope;

  # - Reading line by line from the tips table and compute
  #   score with latest measurement! Therefore we need the latest
  #   measurement first:
  $row    = $DBcon->query("SELECT * FROM meas ORDER BY time DESC LIMIT 1;");
  $latest = $row->fetchArray();
  
  # - First: extracting mean values
  $mean_T    = $DBcon->query("SELECT AVG(T)    AS mean FROM tips WHERE T    > -999.");
  $mean_T    = $mean_T->fetchArray()[0];
  $mean_rh   = $DBcon->query("SELECT AVG(rh)   AS mean FROM tips WHERE rh   > -999.");
  $mean_rh   = $mean_rh->fetchArray()[0];
  $mean_beer = $DBcon->query("SELECT AVG(beer) AS mean FROM tips WHERE beer > -999.");
  $mean_beer = $mean_beer->fetchArray()[0];
  $mean_aero = $DBcon->query("SELECT AVG(aero) AS mean FROM tips WHERE aero > -999.");
  $mean_aero = $mean_aero->fetchArray()[0];

  # - But we need the standard deviation. Therefore: create standard deviation
  function sd($var) {
    global $DBcon;
    $mean = $DBcon->query("SELECT AVG(".$var.") AS mean FROM tips WHERE ".$var." > -999.;");
    $mean = $mean->fetchArray()[0];

    $all  = $DBcon->query("SELECT ".$var." AS var FROM tips WHERE ".$var." > -999.;"); 
    $result = 0; $counter = 0;
    while($row=$all->fetchArray()) {
      #print $row['var']." - ".$mean."<br>";
      $counter++;
      $result = $result + pow($row['var']-$mean,2.);
    }
    # - Square root
    $result = $result / ($counter - 1.);
    $result = pow($result,0.5); 
    return($result);
  }

  # - Compute standard deviation for all variables
  $sd_T    = sd('T');
  $sd_rh   = sd('rh');
  $sd_beer = sd('beer');
  $sd_aero = sd('aero');

  # - Now compute points
  $rows = $DBcon->query("SELECT * FROM tips;");
  while ( $row = $rows->fetchArray() ) {
    # - Temperature 
    if ( $row['T'] > -999. ) { 
      $points = points($latest['T'],$row['T'],$sd_T);
    } else { $points = 0.; }
    $DBcon->exec("UPDATE tips SET p_T = ".$points." WHERE name = '".$row['name']."';");
    # - Relative humidity
    if ( $row['rh'] > -999. ) { 
      $points = points($latest['rh'],$row['rh'],$sd_rh);
    } else { $points = 0.; }
    $DBcon->exec("UPDATE tips SET p_rh = ".$points." WHERE name = '".$row['name']."';");
    # - Beer 
    if ( $row['beer'] > -999. ) { 
      $points = points($latest['beer'],$row['beer'],$sd_beer);
    } else { $points = 0.; }
    $DBcon->exec("UPDATE tips SET p_beer = ".$points." WHERE name = '".$row['name']."';");
    # - Aerosol
    if ( $row['aero'] > -999. ) { 
      $points = points($latest['aero'],$row['aero'],$sd_aero);
    } else { $points = 0.; }
    $DBcon->exec("UPDATE tips SET p_aero = ".$points." WHERE name = '".$row['name']."';");
    # - Mean points
    $mean = $DBcon->query("SELECT p_T,p_rh,p_beer,p_aero FROM tips WHERE name = '".$row['name']."';");
    $points = $mean->fetchArray();
    $points = ( $points['p_T']+$points['p_rh']+$points['p_beer']+$points['p_aero'] ) / 4.;
    $DBcon->exec("UPDATE tips SET p_total = ".$points." WHERE name = '".$row['name']."';");
  }

  // - Now adding RANK. The one with the hightest p_total is
  //   number one
  $rows = $DBcon->query('SELECT name FROM tips ORDER BY p_total DESC');
  $rank = 1;
  while($row = $rows->fetchArray() ) {
    $DBcon->exec("UPDATE tips SET ranking = ".$rank." WHERE name = '".$row['name']."';");
    $rank++;
  }

}


function points($measurement,$tip,$sd)
{
  global $slope;
  global $maxpoints;

  $diff   = abs($measurement - $tip) / $sd;
  $points = $maxpoints - $diff*100/$slope;

  if($points<0) { $points = 0; } 

  return($points);
}

























function create_XML()
{
  global $dbt_tips;
  global $absdir;

  // -- Read entries
  $query = "SELECT * FROM ".$dbt_tips." WHERE active=1 ORDER BY points DESC";
  $result = @mysql_query($query);
  $numrow = @mysql_num_rows($result);
  
  // -- If there are entries -> show
  if($numrow>0)
  {
    // -- Open new xml file
    $fid  = fopen($absdir."/data/values.xml","w+");
    $pfid = fopen($absdir."/data/points.xml","w+");

    // -- Write XML header (fixed format)
    $string  = "<chart>\n\n";
    $string .= "  <chart_data>\n";
    $string .= "    <row>\n";
    $string .= "      <null/>\n";
    $string .= "      <string>Temp</string>";
    $string .= "      <string>RelHum</string>";
    $string .= "      <string>Bier</string>";
    $string .= "      <string>Feinst</string>";
    $string .= "    </row>\n";
    fwrite($fid,$string);
    fwrite($pfid,$string);

    // -- Looping over all entries
    for($i=1;$i<=$numrow;$i++)
    {
      $row = mysql_fetch_array($result);

      // Write Data into value file
      $string  = "    <row>\n";
      $string .= "      <string>".$row['name']."</string>\n";
      $string .= "      <string>".$row['T']."</string>\n";
      $string .= "      <string>".$row['rh']."</string>\n";
      $string .= "      <string>".$row['beer']."</string>\n";
      $string .= "      <string>".$row['aero']."</string>\n";
      $string .= "    </row>\n";
      fwrite($fid,$string);

      // Write Data into value file
      $string  = "    <row>\n";
      $string .= "      <string>".$row['name']."</string>\n";
      $string .= "      <string>".$row['pT']."</string>\n";
      $string .= "      <string>".$row['prh']."</string>\n";
      $string .= "      <string>".$row['pbeer']."</string>\n";
      $string .= "      <string>".$row['paero']."</string>\n";
      $string .= "    </row>\n";
      fwrite($pfid,$string);

    } // End of loop

    // -- Write footer of the xml file
    $string  = "  </chart_data>\n\n";
    $string .= "</chart>\n";
    fwrite($fid,$string);
    fwrite($pfid,$string);

    // -- Close xml file
    fclose($fid);
      
  }
}

?>
