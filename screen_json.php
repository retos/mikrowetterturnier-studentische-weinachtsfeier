<?php
# -------------------------------------------------------------------
# - NAME:        screen_json.php 
# - AUTHOR:      Reto Stauffer
# - DATE:        2013-11-30
# -------------------------------------------------------------------
# - DESCRIPTION: Just converting $screen_* arrays into json for
#                the screen display php scripts.
# -------------------------------------------------------------------
# - EDITORIAL:   2013-11-30, RS: Created file.
#                2013-12-05, RS: Fixed a bug
#                2013-12-08, RS: Added measfoto
# -------------------------------------------------------------------

require('config.php');
require('function.php');

if ( empty($_GET) ) { $_GET['what'] = 'tables'; } # setting default
if ( $_GET['what'] == 'tables' ) {
  print json_encode(array("speed"=>$screen_tables_speed,"pages"=>$screen_tables));
} else if ( $_GET['what'] == 'all' ) {
  print json_encode(array("speed"=>$screen_all_speed,"pages"=>$screen_all));
// - Measurements and Fotos
} else if ( $_GET['what'] == 'measfoto' ) {
  print json_encode(array("speed"=>$screen_measfoto_speed,"pages"=>$screen_measfoto));
// - Measurements only
} else {
  print json_encode(array("speed"=>$screen_meas_speed,"pages"=>$screen_meas));
}

?>
