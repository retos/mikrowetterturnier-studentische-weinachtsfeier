<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta charset="UTF-8" />
<title>Mikrowetterturnier</title>
<head>
<!-- Include stylefile -->
<link href='style.css' rel='stylesheet' type='text/css'>
<?php

# - Loading necessary config file and function library
require("config.php");
require("function.php");

# - Open connection to the sqlite database. 
#   If the file does not exist: create database and insert
#   necessary tables.
$DBcon = DBconnect();

# -------------------------------------------------------------------
# - Checking if $_POST is not empty. If it is not empty,
#   check the 'action' variable and do some stuff.
#   new:      insert a new tip into the database
#   meas:     insert a new measurement into the database
#   message:  gib a message output
# ----------
$message = NULL; # default value, no message
$message_class = 'default'; # class of the message (controls optic)
if(! empty($_POST) ) {
    # --------------------------------------------------------------
    # - New entry to the tips table
    # ----------
    if($_POST['action']=='new')
    {
      $message = inserttip();
    }
    # --------------------------------------------------------------
    # - New entry to the measurement table
    # ----------
    elseif($_POST['action']=="meas")
    {
      $message = insertmeas();
    }
    # --------------------------------------------------------------
    # - Just an information
    # ----------
    elseif($_GET['message']=="rank")
    {
      echo "* New ranking computed.";
    }
}

?>
</head>
<body>

<div id='column-left'>
  <?php
  # - Show message if there is one ($message not equal to NULL)
  if ( ! is_null($message) ) {
    echo "<div id='message' class='".$message['class']."'>".$message['text']."</div>";
  }
  ?>
  
  <!-- Form to insert new entry -->
  <form name='new' method='post' action='<?php print $_SERVER['PHP_SELF']; ?>'>
  <h3>Neuen Mitspielertip eingeben</h3>
  <ul>
    <li>
      <idesc>Name:</idesc>
      <input type='text' name='name' value='<?php showval('name'); ?>'></input></li>
    </li>
    <li>
      <idesc>Temperatur:</idesc>
      <input type='text' name='T' value='<?php showval('T'); ?>'></input>
    </li>
    <li>
      <idesc>Rel.Feuchte:</idesc>
      <input type='text' name='rh' value='<?php showval('rh'); ?>'></input>
    </li>
    <li>
      <idesc>Bierkonsum:</idesc>
      <input type='text' name='beer' value='<?php showval('beer'); ?>'></input>
    </li>
    <li>
      <idesc>Aerosol:</idesc>
      <input type='text' name='aero' value='<?php showval('aero'); ?>'></input>
    </li>
    <li>
      <idesc>Gruppe:</idesc>
      <?php
        if ( ! empty($_POST['typ']) ) { $warning = "<span class='warning'>WARNING: default value!</span>"; }
        else { $warning = NULL; }
      ?>
      <select name='typ'>
        <option value='1' selected>[1] Irgendwer</option>
        <option value='2'>[2] Meteostudent</option>
        <option value='3'>[3] IMGI Mitarb</option>
      </select>
      <?php if ( ! is_null($warning) ) { print $warning; } ?>
    </li>
    <li><idesc>&nbsp;</idesc><input type='submit' name='submit' value='save'></input></li>
    <li><idesc>&nbsp;</idesc><input type='hidden' name='action' value='new'></input></li>
  </ul>
  </form>
  
  <!-- Form to insert new meassurement -->
  <form name='new' method='post' action='<?php print $_SERVER['PHP_SELF']; ?>'>
  <h3>Neue Messung eingeben</h3>
  <ul>
    <li>
      <idesc>Time (HH:MM):</idesc>
      <input type='text' name='mtime' value='<?php showval('mtime'); ?>'></input>
    </li>
    <li>
      <idesc>Temperatur:</idesc>
      <input type='text' name='mT' value='<?php showval('mT'); ?>'></input>
    </li>
    <li>
      <idesc>Rel.Feuchte:</idesc>
      <input type='text' name='mrh' value='<?php showval('mrh'); ?>'></input>
    </li>
    <li>
      <idesc>Bierkonsum:</idesc>
      <input type='text' name='mbeer' value='<?php showval('mbeer'); ?>'></input>
    </li>
    <li>
      <idesc>Aerosol:</idesc>
      <input type='text' name='maero' value='<?php showval('maero'); ?>'></input>
    </li>
    <li><idesc>&nbsp;</idesc>        <input type='submit' name='submit' value='save'></input></li>
    <li><idesc>&nbsp;</idesc>        <input type='hidden' name='action' value='meas'></input></li>
  </ul>
  </form>

  [<a href='computepoints.php'>Rechne aktuelle Punkte!</a>]<br>

  <h3>Screen Praesentations</h3>
  Tip: kann in config.conf umkonfiguriert werden. Sowohl der speed
  als auch die Inhalte, die angezeigt werden.<br>
  [<a target="_new" href='screen.php?what=meas'>Loop ueber Messungen!</a>]<br>
  [<a target="_new" href='screen.php?what=measfoto'>Loop ueber Messungen! + Fotos</a>]<br>
  [<a target="_new" href='screen.php?what=tables'>Loop ueber Tabellen!</a>]<br>
  [<a target="_new" href='screen.php?what=all'>Alles moegliche!</a>]<br>
  <h3>Foto Gallery</h3>
  Random-Auswahl aller Bilder in <i>fotos/</i>. Koennen im
  Breitbild oder Hochformat sein.<br>
  [<a target="_new" href='fotos.php?n=1000'>Fotogalerie</a>]<br>
  
</div>
<div id='column-right'>
  <?php
  // -- Show all stored measurements
  showmeas();
  showtips();
  ?>
</div>

<?php
// -- Close database
DBclose();
?>
</body>
</html>
