<?php
# -------------------------------------------------------------------
# - NAME:        getjson.php
# - AUTHOR:      Reto Stauffer
# - DATE:        2013-11-31
# -------------------------------------------------------------------
# - DESCRIPTION: Loading different json-arrays from the database
#                to visualize them via highcharts (js).
#
# - OPTIONS:
#   getjson.php?what=best         gives you "n" best players. "n" is
#                                 defined in the config.php file as
#                                 "$json_defaultn"
#   getjson.php?what=best&n=20
# -------------------------------------------------------------------

# - Requires basic functions
require("config.php");
require("function.php");


# - Connecting to the sqlite3 database
$DBcon = DBconnect();

# -------------------------------------------------------------------
# - If $_GET is empty: stop
if ( empty($_GET) ) {
  die("Sorry, I have no fucking idea what you would like to know! Stop.");
}

# -------------------------------------------------------------------
# - If $_GET['what'] is empty, setting "$json_whatdefault" as
#   default value.
if ( !empty($_GET['what']) ) { $what = $_GET['what']; }
else                         { $what = $json_whatdefault; }


# -------------------------------------------------------------------
# - If there is an input called "n" use this one, else
#   use the default "n" from the config file.
if ( !empty($_GET['n']) ) { $n = (int)$_GET['n']; }
else                      { $n = (int)$json_ndefault;  }

# -------------------------------------------------------------------
# - Else there are a few different "hashes" and inputs you can ask for.
#   Please have a look to the header to see what's scripted right now.

# - SELECTING: best (overall best points)
if ( $what == 'bar_best' | $what == 'bar_worst' | $what == 'bar_random' ) {
  # - Select "n" best players from the database, store as arrays
  if      ( $what == 'bar_best' )  { $order = 'p_total DESC'; }
  else if ( $what == 'bar_worst' ) { $order = 'p_total ASC'; }
  else                             { $order = 'RANDOM()'; }
  $rows = $DBcon->query('SELECT * FROM tips ORDER BY '.$order.' LIMIT '.$n);
  # - Empty arrays to store the data
  $result = array();
  $res_name  = array();
  $p_T       = array();
  $p_rh      = array();
  $p_beer    = array();
  $p_aero    = array();
  $p_total   = array();
  $ranking   = array();
  # - Put data into the arrays
  while($row = $rows->fetchArray()) {
    array_push($res_name,$row['name']);
    array_push($p_T,   $row['p_T']);
    array_push($p_rh,  $row['p_rh']);
    array_push($p_beer,$row['p_beer']);
    array_push($p_aero,$row['p_aero']);
    array_push($p_total,round($row['p_total'],2));
    array_push($ranking,(int)$row['ranking']);
  }
  $result = array('name'=>$res_name,'p_T'=>$p_T,'p_rh'=>$p_rh,
                  'p_beer'=>$p_beer,'p_aero'=>$p_aero,'p_total'=>$p_total,
                  'ranking'=>$ranking);

# -------------------------------------------------------------------
} else if ( $what == 'best' ) {
  # - Select "n" best players from the database, store as arrays
  $rows = $DBcon->query('SELECT * FROM tips ORDER BY p_total LIMIT '.$n);
  # - Empty arrays to store the data
  $name    = array();
  $p_T     = array();
  $p_rh    = array();
  $p_beer  = array();
  $p_aero  = array();
  $p_total = array();
  # - Put data into the arrays
  while($row = $rows->fetchArray()) {
    array_push($name,$row['name']);
    array_push($p_T,$row['p_T']);
    array_push($p_rh,$row['p_rh']);
    array_push($p_beer,$row['p_beer']);
    array_push($p_aero,$row['p_aero']);
    array_push($p_total,$row['p_total']);
  }
  # - Create array for the json encoding.
  $result = array('name'=>$name,'p_T'=>$p_T,'p_rh'=>$p_rh, 
                  'p_beer'=>$p_beer,'p_aero'=>$p_aero);
# -------------------------------------------------------------------
} else if ( $what == 'meas' ) {
  # - Loading all T measurements from the database 
  $rows = $DBcon->query('SELECT * FROM meas ORDER BY time ASC');
  # - Empty arrays to store the data
  $res_T    = array();
  $res_rh   = array();
  $res_beer = array();
  $res_aero = array();
  while($row = $rows->fetchArray()) {
    $time = $row['time']*1000.;
    array_push($res_T,   array($time,$row['T']));
    array_push($res_rh,  array($time,$row['rh']));
    array_push($res_beer,array($time,$row['beer']));
    array_push($res_aero,array($time,$row['aero']));
  }
  $result = array("meas_T"=>$res_T,"meas_rh"=>$res_rh,
                  "meas_beer"=>$res_beer,"meas_aero"=>$res_aero);
# -------------------------------------------------------------------
} 





















# - Return json array (or print_r if "dev" (development) flag is set)
if ( empty($_GET['dev']) ) {
  print json_encode($result);
} else {
  print_r($result);
}


# - Close database connection
DBclose();
?>
