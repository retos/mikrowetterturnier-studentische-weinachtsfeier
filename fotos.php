<!DOCTYPE html>
<?php
require('function.php');
require('config.php');

# - If there is an input argument ?n=X we take
#   X random images.
if ( ! empty($_GET) ) {
  if ( ! empty($_GET['n']) ) {
    $numberofimages = (int)$_GET['n'];
  }
}

# - Searching for valid images in images directory,
#   takes a random one and displays it.
$images = array();
if ($handle = opendir($imagesdir)) {
  while (false !== ($file = readdir($handle))) {
    if ($file != "." && $file != "..") {
      # - Extracting file sufix. Ony a certain
      #   type of images is allowed. Please have a look
      #   to config.php to see which ones. Note that
      #   those are not case sensitive (png == PNG)
      $sufix = strtolower(explode('.',$file)[1]);
      if ( in_array($sufix,$valid_images) ) {
        array_push($images,$file);
      }
    }
  }
  closedir($handle);
}

# - Now we have to return a random image out of the
#   array $image
function rand_best($min, $max) {
    $generated = array();
    for ($i = 0; $i < 100; $i++) {
        $generated[] = mt_rand($min, $max);
    }
    shuffle($generated);
    $position = mt_rand(0, 99);
    return $generated[$position];
}
# - do we have to take random number of images?
#   or are there not enough to take $numberofimages
#   out of it?
$which = array();
if ( (int)$numberofimages < (int)count($images) ) {
  for($i=0;$i<$numberofimages;$i++) {
    array_push($which,rand_best(0,count($images)-1));
  }
} else {
  for($i=0;$i<count($images);$i++) {
    array_push($which,rand_best(0,count($images)-1));
  }
}

$myimages = array();
For($i=0;$i<count($which);$i++) {
  array_push($myimages,$images[$which[$i]]);
}

?>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/jquery.maximage.css">
<style type='text/css' media="screen">          
body {
  background-color: black;
  overflow: hidden;
}
#maximage {
    display:none;
    position:fixed !important;
}
</style>

<!-- including latest jquery.min -->
<script src='js/jquery-1.10.2.min.js'></script>

<!-- maximage jquery gallery plugin -->
<script src='js/jquery.cycle.all.min.js'></script>
<script src='js/jquery.maximage.min.js'></script>

<!-- script/function to start the maximage plugin -->
<script type='text/javascript'>
$(document).ready( function() {

  //$('#maximage').maximage({
  //  cycleOptions: {
  //    fx:'scrollHorz',
  //    speed: 800,
  //    timeout: 8000,
  //    prev: '#arrow_left',
  //    next: '#arrow_right'
  //  },
  //  onFirstImageLoaded: function(){
  //    jQuery('#cycle-loader').hide();
  //    jQuery('#maximage').fadeIn('fast');
  //  }
  //});
  // Trigger maximage
  $.maximage_init = function() {
    // Trigger maximage
    $('#maximage').maximage({
      cssBackgroundSize: false, // We don't want to use background-size:cover for our custom size
      backgroundSize: function( $item ){
        // Contain Portrait
        if ($item.data('h') > $item.data('w')) {
          if ($.Window.data('w') / $.Window.data('h') < $item.data('ar')) {
            $item
                .height(($.Window.data('w') / $item.data('ar')).toFixed(0))
                .width($.Window.data('w'));
          } else {
            $item
                .height($.Window.data('h'))
                .width(($.Window.data('h') * $item.data('ar')).toFixed(0));
          }
        } else {
          if ($.Window.data('w') / $.Window.data('h') < $item.data('ar')) {
            $item
                .height($.Window.data('h'))
                .width(($.Window.data('h') * $item.data('ar')).toFixed(0));
          } else {
            $item
                .height(($.Window.data('w') / $item.data('ar')).toFixed(0))
                .width($.Window.data('w'));
          }
        }
      },
      onImagesLoaded: function(){
          $('#maximage').fadeIn();
      }
    });
  };
  $.maximage_init();

  // - When changing window size
  $(window).resize( function() { $.maximage_init(); });

});

</script>

</head>
<body>
<div id="maximage">
    <?php // - Print image tags
    for($i=0;$i<count($myimages);$i++) {
      if ( $i == 0 ) {
        echo "    <img src='".$relimagesdir."/".$myimages[$i]."'></img>\n";
      } else {
        echo "    <img src='".$relimagesdir."/".$myimages[$i]."'></img>\n";
      }
    }
    ?>
</div>
</body>
</html>
