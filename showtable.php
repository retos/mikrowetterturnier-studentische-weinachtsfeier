<?php 
# - Include config file to get the color definition
require('config.php');

# - If $_GET is empty: take default (best 5)
if ( empty($_GET) ) {
  $what = 'bar_best';
  $nn   = $json_ndefault;
} else {
  $what = 'bar_best'; # default
  $nn   = $json_ndefault;
  # - If user settings: use them
  if ( ! empty($_GET['what']) ) { $what = $_GET['what']; }
  if ( ! empty($_GET['nn']) )   { $nn   = (int)$_GET['nn']; }
}
?>
<!DOCTYPE html>
<html>
<head>

<script src='js/jquery-1.10.2.min.js'></script>

<style type='text/css'>
body {
  background-color: black;
}
* {
  color: white;
  font-family: Arial, Verdana, sans-serif;
  font-size: 30px;
}

/* Data table styling */
#data {
  display: block;
  position: relative;
  left: 50%;
  width: 750px;
  margin-left: -375px;
}

.tdc { text-align: center; }
.tdl { text-align: left;   }
.tdr { text-align: right;  }

.tdranking { width: 80px;  }
.tdname    { width: 550px;  }
.tdpoints  { width: 120px;  }

td.tdranking { font-weight: bold; }
td.tdname    { font-weight: bold; font-style: italic; }

tr.col-gray { background-color: #424242; }

.datarow { display: none; }

/* Header styling */
#title {
  display: block;
  position: relative;
  left: 50%;
  width: 750px;
  margin-left: -375px;
  text-align: center;
}
  
</style>

<script type='text/javascript'>
$(document).ready(function(){

  var nn = '<?php print $nn; ?>';
  var what = '<?php print $what; ?>';

  var title = 'Die Besten '; var reverse = true;
  if      ( what == 'bar_worst' )  { reverse = false; title = 'Die Schlechtesten'; }
  else if ( what == 'bar_random' ) { reverse = false; title = 'Zufaellige'; }

  // - Adding title to the body
  $("body").append("<h3 id='title'>"+title+" "+nn+" Spieler</h3>");


  // - A small helper function to add table head
  $.addheader = function() {
    $("#data").append("<tr>\n"
                     +"  <th class='tdc tdranking'>Rang</th>\n"
                     +"  <th class='tdl tdname'>Name</th>\n"
                     +"  <th class='tdr tdpoints'>Punkte</th>\n"
                     +"</tr>\n");
  }
  // - A small helper function to add the data
  $.adddata = function(reverse) {
    for(var i=0; i<$.jsondata.name.length; i++) {
      if ( ! reverse ) { j = i }
      else { j = $.jsondata.name.length - 1 - i; }
      if ( j == Math.floor(j/2.)*2 ) { var rowcol = 'col-gray'; }
      else                           { var rowcol = 'col-black'; }
      $("#data").append("<tr class='datarow "+rowcol+"' id='datarow-"+i+"'>\n"
                       +"  <td class='tdc tdranking'>"+$.jsondata.ranking[j]+"</td>\n"
                       +"  <td class='tdl tdname'>"+$.jsondata.name[j]+"</td>\n"
                       +"  <td class='tdr tdpoints'>"+$.jsondata.p_total[j]+"</td>\n"
                       +"</tr>\n");
    }
  }

  // - Loading data via .ajax
  $.ajax({
    url: "getjson.php", 
    data: {'what':what,'n':nn},
    dataType: "json",
    async: false,
    success: function(data) {
      $.jsondata = data
      // - Build table, all rows are hidden
      $("body").append("<table id='data'></table>");
      $.addheader();
      $.adddata(reverse);
    }
  });

  // - Setting counter to 0 first
  $.counter = 0;
  // - This is the function which highlights the single rows
  $.myfun = function() {
    $("#datarow-"+$.counter).fadeIn();
    if ( $.counter >= $.jsondata.name.length ) { clearInterval($.interval); }
    $.counter++
  }

  // - Adding rows, delay 2 seconds
  $.interval = window.setInterval($.myfun,2000);

});
</script>

</head>
<body>
</body>
</html>
