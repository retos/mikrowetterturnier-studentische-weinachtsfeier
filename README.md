

Mikrowetterturnier fuer die Studenten-Weihnachtsfeier
=====================================================
Script fuer die Studenten-Weihnachtsfeier

SQLite3 Database
================
Die Daten werden nun alle in eine sqlite Datenbank
abgelegt (nicht mehr mysql Datenbank). Alte Files
koennen aufbewahrt werden, da jedes Jahr sowieso
automatisch ein neues File (Jahr im Namen) angelegt
wird.

Wichtig
=======
Der Apache-Webserver-User braucht schreibrechte und 
leserechte im "sqlite" Verzeichnis!
Variante a): Rechte sauber vergeben.
Variante b): Einfach das sqlite file loeschen, der
Server legt dieses dann selber an, wodurch auch die
Rechte wieder passen.
